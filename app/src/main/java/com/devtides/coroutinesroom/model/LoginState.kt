package com.devtides.coroutinesroom.model

object LoginState {
    var isLoggedIn=false
    var user: User? = null

    fun logOut(){
        isLoggedIn = false
        user = null
    }

    fun logIn(user:User){
        isLoggedIn = true
        this.user = user
    }
}