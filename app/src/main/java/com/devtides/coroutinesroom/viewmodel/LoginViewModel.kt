package com.devtides.coroutinesroom.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.devtides.coroutinesroom.model.LoginState
import com.devtides.coroutinesroom.model.LoginState.user
import com.devtides.coroutinesroom.model.User
import com.devtides.coroutinesroom.model.UserDatabase
import kotlinx.coroutines.*

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    private val db by lazy { UserDatabase(getApplication()).userDao() }

    val loginComplete = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()

    fun login(username: String, password: String) {

        coroutineScope.launch {
            val user = db.getUser(username)
            if (user==null) {
                withContext(Dispatchers.Main) {
                    error.value = "User does not exist"
                }
            }else if(user.passwordHash!=password.hashCode()){
                withContext(Dispatchers.Main){
                    error.value = "Incorrect Password"
                }
            }else{
                LoginState.logIn(user)
                withContext(Dispatchers.Main){
                    loginComplete.value=true
                }
            }
        }
    }
}